<?php
#called by a dictionary to complete the dictionary functions
	
	
	function shrink($text){
		global $d;
		$from=array_keys($d);
		$to=array_values($d);
		return (str_replace($from,$to,$text));
	}
	
	function stretch($text){
		global $d;
		$to=array_keys($d);
		$from=array_values($d);
		return (str_replace($from,$to,$text));
	}
	
	
	$encode=shrink('42 Oakdene Road, Sevenoaks, Kent, TN13 3HL');
	echo $encode;
	$encode=stretch($encode);
	echo "\n";
	echo $encode;
	
	echo "\n -=loaded dictionary=- \n";
	